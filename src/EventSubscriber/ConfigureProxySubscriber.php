<?php

namespace Drupal\saml_sp\EventSubscriber;

use Drupal\Core\Site\Settings;
use OneLogin\Saml2\Utils;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Check if a proxy is used and configure OneLogin\Saml2\Utils accordingly.
 */
class ConfigureProxySubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = 'onRequest';

    return $events;
  }

  /**
   * Configure the proxy settings of OneLogin\Saml2\Utils.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The request event.
   */
  public function onRequest(RequestEvent $event) {
    Utils::setProxyVars(Settings::get('reverse_proxy', FALSE));
  }

}
